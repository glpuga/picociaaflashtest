
DUMP_SIZE := 4096

CFG_FILE := etc/lpc54102_cd.cfg

GDB_FLASH_DUMP_SCRIPT_FILE := etc/gdbmemdumpscript

RESULTS_PATH := results/

INFO_CMD := $(OOCD_PATH)openocd -f $(CFG_FILE) -c "init" -c "reset halt" -c "flash banks" -c "flash info 0" -c "reset run" -c "shutdown"

DOWNLOAD_CMD := $(OOCD_PATH)openocd -f $(CFG_FILE) -c "init" -c "reset halt" -c "flash write_image erase unlock data/testsequence.bin 0x00000000 bin" -c "reset run" -c "shutdown"

ERASE_CMD := $(OOCD_PATH)openocd -f $(CFG_FILE) -c "init" -c "reset halt" -c "flash protect 0 0 last off" -c "flash erase_sector 0 0 last" -c "reset run" -c "shutdown"

ERASE_CHECK_CMD := $(OOCD_PATH)openocd -f $(CFG_FILE) -c "init" -c "reset halt" -c "flash erase_check 0" -c "reset run" -c "shutdown"

FLASH_DUMP_CMD := $(OOCD_PATH)openocd -f $(CFG_FILE) -c "init" -c "reset halt" -c "dump_image full_dump.bin 0 $(DUMP_SIZE)" -c "reset run" -c "shutdown"

HEXDUMP_CMD := hexdump -v -C full_dump.bin

donothing:
	echo Options ares "downoad" or "erase"

info:
	@$(INFO_CMD)

erase:
	@$(ERASE_CMD)

download:
	@$(DOWNLOAD_CMD)

erase_check:
	@$(ERASE_CHECK_CMD)

flash_dump:
	@$(FLASH_DUMP_CMD)
	@$(HEXDUMP_CMD)

full_test: 
	@$(INFO_CMD)
	
	@$(ERASE_CMD)
	@$(ERASE_CHECK_CMD)
	@$(FLASH_DUMP_CMD)
	cp full_dump.bin  $(RESULTS_PATH)0_flash_image_first_erase.bin
	@$(HEXDUMP_CMD) > $(RESULTS_PATH)0_flash_image_first_erase.txt
	
	@$(DOWNLOAD_CMD)
	@$(ERASE_CHECK_CMD)
	@$(FLASH_DUMP_CMD)
	cp full_dump.bin  $(RESULTS_PATH)1_flash_image_programmed_state.bin
	@$(HEXDUMP_CMD) > $(RESULTS_PATH)1_flash_image_programmed_state.txt
	
	@$(ERASE_CMD)
	@$(ERASE_CHECK_CMD)
	@$(FLASH_DUMP_CMD)
	cp full_dump.bin  $(RESULTS_PATH)2_flash_image_second_erase.bin
	@$(HEXDUMP_CMD) > $(RESULTS_PATH)2_flash_image_second_erase.txt
