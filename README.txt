
# make full_test 

Realiza una secuencia de ensayo completa de la memoria flash
de lpc54102, consistente en los siguientes pasos:

1) Borrado completo de la memoria flash.
2) Descarga del contenido completo de la memoria y almacenamiento
   en 0_flash_image_first_erase.txt .
3) Sobreescritura de la memoria flash con el patrón que se 
   encuentra en data/testsequence.bin
4) Descarga del contenido completo de la memoria y almacenamiento
   en 1_flash_image_programmed_state.txt .
5) Borrado completo de la memoria flash.
6) Descarga del contenido completo de la memoria y almacenamiento
   en 2_flash_image_second_erase.txt

---

Otros targets:

make info        Muestra información acerca de los bancos de memoria.
make erase       Borra el contenido de la memoria flash.
make download    Sobre escribe la memoria flash completa con un patrón
                 que se puede encontrar en data/testsequence.bin 
make flash_dump  Realiza un volcado del contenido completo de la memoria 
                 flash al archivo full_dump.bin .
make erase_check Verifica el estado de borrado de la memoria utilizando el 
                 comando de OpenOCD correspondiente.
